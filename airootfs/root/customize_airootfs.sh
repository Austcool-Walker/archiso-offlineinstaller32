#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

! id liveuser && useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/zsh liveuser
#injecting Desktop settings for LiveUser:
git clone https://gitlab.com/Austcool-Walker/liveuser-desktop-settings32.git
cd liveuser-desktop-settings32
rm -v -R /home/liveuser/.config
cp -v -R .config /home/liveuser/
cp -v -R .cinnamon /home/liveuser/
cp -v -R .local /home/liveuser/
cp -v -R .cache /home/liveuser/
cp -v -R .gnupg /home/liveuser/
cp -v -R .icons /home/liveuser/
#cp -v -R .themes /home/liveuser/
#cp -v -R Desktop /home/liveuser/
#cp -v -R Documents /home/liveuser/
cp -v -R Templates /home/liveuser/
#cp -v -R Pictures /home/liveuser/
#cp -v  strap.sh /home/liveuser/
cp -v  .profile /home/liveuser/
cp -v  .dir_colors /home/liveuser/
cp -v  .face /home/liveuser/
cp -v .face.icon /home/liveuser/
chown -v -R liveuser:users /home/liveuser/.config
cp -v install /home/liveuser/
chmod -v +x /home/liveuser/install
chown -v liveuser:users /home/liveuser/install
cp -v preinfo.txt /home/liveuser/
chown -v liveuser:users /home/liveuser/preinfo.txt
cd .. 
rm -v -R liveuser-desktop-settings32
#
chmod 755 /etc/sudoers.d
mkdir -p /media
chmod 755 /media
chmod 440 /etc/sudoers.d/g_wheel
#chmod 644 /etc/systemd/system/*.service
chown 0 /etc/sudoers.d
chown 0 /etc/sudoers.d/g_wheel
chown root:root /etc/sudoers.d
chown root:root /etc/sudoers.d/g_wheel
chmod 755 /etc

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl enable NetworkManager.service mbpfan.service lightdm.service
systemctl set-default multi-user.target
